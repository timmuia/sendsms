package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

var message string
var phone string
var userName = "254722000001"
var password = "EGP#69"

const base_url = "https://sms-service.intouchvas.io"

type TokenResponse struct {
	Token    string `json:"token"`
	Lifetime uint   `json:"lifetime"`
}

func main() {

	sendSMS()
}

func getToken() string {

	var tokenUrl = "https://identity-service.intouchvas.io/auth/api-key"

	client := &http.Client{}

	// Create a new GET request
	req, err := http.NewRequest("GET", tokenUrl, nil)
	if err != nil {
		fmt.Println("Failed to create request:", err)
		return ""
	}

	// Add headers to the request
	req.Header.Add("Content-Type", "application/json")

	authorization := userName + ":" + password
	authorization = base64.StdEncoding.EncodeToString([]byte(authorization))
	req.Header.Add("Authorization", "Basic "+authorization)

	// Send the request
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Failed to send request:", err)
		return ""
	}
	defer resp.Body.Close()

	var token TokenResponse

	error := json.NewDecoder(resp.Body).Decode(&token)

	if error != nil {
		fmt.Printf("Error: ", error)
	}

	fmt.Println("Response body:", token.Token)

	return token.Token

}

func sendSMS() {

	smsUrl := base_url + "/message/send/transactional"

	//Get authorized API key
	apiKey := getToken()

	//Constructing JSON Data to send
	newSmsData := []byte(`{"message": "This is a test message","msisdn": "25471xxxxxxx","sender_id":"INTOUCHVAS"}`)

	request, err := http.NewRequest("POST", smsUrl, bytes.NewBuffer(newSmsData))

	if err != nil {
		fmt.Println("Error sending SMS:", err)
		return
	}

	//Appending required headers to the post request
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("api-key", apiKey)

	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		fmt.Println("Error sending request:", err)
		return
	}
	defer response.Body.Close()

	//reading response
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error reading response:", err)
		return
	}

	fmt.Println("Response body:", string(body))

}
